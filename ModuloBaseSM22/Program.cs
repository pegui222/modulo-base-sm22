﻿using System;
using ModuloBaseSM22.Clases;

namespace ModuloBaseSM22
{
    class Program
    {
        static void Main(string[] args)
        {
            Persona persona1 = new Persona();
            persona1.Nombre = "AdonisJS";
            persona1.SueldoMensual = 25000;

            Console.WriteLine($"Su nombre es: {persona1.Nombre}");
            Console.WriteLine($"Su sueldo es: {persona1.SueldoMensual}");
            Console.WriteLine($"Su sueldo anual es: {persona1.SueldoAnual}");

            CambiarNombre(persona1);

            Console.WriteLine($"Su nuevo nombre es: {persona1.Nombre}");

            int numero = 1;

            Console.WriteLine("El numero es: " + numero);

            CambiarNumero(ref numero);

            Console.WriteLine("El nuevo numero es: " + numero);
        }

        static void CambiarNombre(Persona persona)
        {
            persona.Nombre = "Hau Pech";
        }

        static void CambiarNumero(ref int numero)
        {
            numero = 20;
        }
    }
}
